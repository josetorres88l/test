﻿<%@ Page Title="" Language="C#"  AutoEventWireup="true" CodeBehind="Index.aspx.cs" Inherits="WebLayer.View.Index" %>
<%@ Register assembly="DevExpress.Web.v15.2, Version=15.2.9.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web" tagprefix="dx" %>


    <form runat="server">    
        <table class="OptionsTable" align="center" runat="server" id="serverContainer" > 
        <tr>
                <td style="width: 30px;"> 
                      <dx:ASPxTextBox ID="AId" runat="server" Width="170px" Height="30px" style="display: inline-block;" NullText="ID"  ></dx:ASPxTextBox>
                </td>
                <td style="height:50px;">
                    <dx:ASPxTextBox ID="ANombre" runat="server" Width="170px" Height="30px" style="display: inline-block" NullText="Nombre"></dx:ASPxTextBox>
                </td>   
            </tr>
        
            <tr>
                <td style="width: 30px;"> 
                    <dx:ASPxTextBox ID="AApellido" runat="server" Width="170px" Height="30px" style="display: inline-block" NullText="Apellido"></dx:ASPxTextBox>
                </td>
                <td style="height:50px;">
                    <dx:ASPxTextBox runat="server" EnableClientSideAPI="True" Width="170px" Height="30px" ID="AEmail" ClientInstanceName="Email" style="display: inline-block" NullText="Correo">
                        <ValidationSettings SetFocusOnError="True">
                            <RegularExpression ErrorText="Invalid e-mail" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" />
                            <RequiredField IsRequired="False" ErrorText="E-mail is required" />
                        </ValidationSettings>
                        <InvalidStyle BackColor="LightPink" />
                    </dx:ASPxTextBox>
                </td>
            </tr>
            <tr>
                <td style="height: 50px;"> 
                    <dx:ASPxTextBox ID="AEdad" runat="server" Width="170px" Height="30px" style="display: inline-block" NullText="Edad"></dx:ASPxTextBox>
                </td>
                <td style="width: 50px; height:50px;"> 
                <dx:ASPxButton ID="Buscar" runat="server" Text="Buscar" Theme="iOS" OnClick="Buscar_Click" HorizontalAlign="Right" RightToLeft="False"  VerticalAlign="Middle" Wrap="True">
                    <Paddings Padding="5px" />
                    <Border BorderColor="#AAC46C" BorderStyle="Solid" BorderWidth="3px" />
                    </dx:ASPxButton>
                </td>
            </tr>
             <tr>
                <td style="width: 50px;"> 
                </td>
            </tr>
        </table>   
  
 
    
    
<table align="center">
    <tr>
        <td>
             <dx:ASPxGridView ID="Grid" runat="server" Theme="Office2003Olive" Width="400px">
      
            </dx:ASPxGridView>
        
        </td>
     </tr>
    </table>

<table align="center">
    <tr>
        <td >
            <dx:ASPxButton ID="Insertar" runat="server" Text="Insertar" Theme="iOS" HorizontalAlign="Left"  style="display: inline-block" OnClick="Insertar_Click"> <Border BorderColor="#AAC46C" BorderStyle="Solid" BorderWidth="3px" /></dx:ASPxButton>
        </td>
        <td >
            <dx:ASPxButton ID="Eliminar" runat="server" Text="Borrar" Theme="iOS" style="display: inline-block"  OnClick="Eliminar_Click"> <Border BorderColor="#AAC46C" BorderStyle="Solid" BorderWidth="3px" /></dx:ASPxButton>
        </td>
        <td >
            <dx:ASPxButton ID="Detalle" runat="server" Text="Ver Detalle" Theme="iOS" HorizontalAlign="Center"  style="display: inline-block" OnClick="Detalle_Click"> <Border BorderColor="#AAC46C" BorderStyle="Solid" BorderWidth="3px" /></dx:ASPxButton>
        </td>
      </tr>
      <tr>
            <td style="height: 50px;">
            </td>
        </tr>
</table>
        
         <table class="OptionsTable" align="center" runat="server" id="Table1" border="1" visible="false">   
            <tr >
                <td >

                <dx:ASPxLabel ID="ASPxLabel1" runat="server"  Text="Crear Editar y Eliminar"></dx:ASPxLabel>
                </td>
            </tr>      
            <tr>   
                <td style="width: 30px;"> 
                      <dx:ASPxTextBox ID="Aid2" runat="server" Width="170px" Height="30px"  NullText="ID" Enabled="False"  ></dx:ASPxTextBox>
                </td>  
             
            </tr>
            <tr>
                 <td style="height:50px;">
                    <dx:ASPxTextBox ID="Anombre2" runat="server" Width="170px" Height="30px"  NullText="Nombre"></dx:ASPxTextBox>
                </td> 
            </tr>

            <tr>
                <td style="width: 30px;"> 
                    <dx:ASPxTextBox ID="Aapellido2" runat="server" Width="170px" Height="30px"  NullText="Apellido"></dx:ASPxTextBox>
                </td>
            </tr>
            <tr>
                <td style="height:50px;">
                    <dx:ASPxTextBox runat="server" EnableClientSideAPI="True" Width="170px" Height="30px" ID="Acorreo2" ClientInstanceName="Email" style="display: inline-block" NullText="Correo">
                        <ValidationSettings SetFocusOnError="True">
                            <RegularExpression ErrorText="Invalid e-mail" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" />
                            <RequiredField IsRequired="False" ErrorText="E-mail is required" />
                        </ValidationSettings>
                        <InvalidStyle BackColor="LightPink" />
                    </dx:ASPxTextBox>
                </td>
            </tr>
            <tr>
                <td style="height: 50px;"> 
                    <dx:ASPxTextBox ID="Aedad2" runat="server" Width="170px" Height="30px"  NullText="Edad"></dx:ASPxTextBox>
                </td>
            </tr>
       
        </table>   

<table class="OptionsTable" align="center" runat="server" id="Table2"  visible="false">
    <tr>
        <td >
            <dx:ASPxButton ID="Guardar"  runat="server" Text="Guardar" Theme="iOS" HorizontalAlign="Left" style="display: inline-block" OnClick="Guardar_Click" > <Border BorderColor="#AAC46C" BorderStyle="Solid" BorderWidth="3px"  /></dx:ASPxButton>
        </td>
        <td >
            <dx:ASPxButton ID="Limpiar"   runat="server" Text="Salir" Theme="iOS" style="display: inline-block" OnClick="Salir_Click" > <Border BorderColor="#AAC46C" BorderStyle="Solid"  BorderWidth="3px"  /></dx:ASPxButton>
        </td>
        <td >
            <dx:ASPxButton ID="Borrar" runat="server" Text="Borrar" Theme="iOS" HorizontalAlign="Center" style="display: inline-block" OnClick="Borrar_Click" > <Border BorderColor="#AAC46C" BorderStyle="Solid"  BorderWidth="3px"   /></dx:ASPxButton>
        </td>
      </tr>
</table>  
</form>
    
         
       

    