﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Utility.Utility;
using BusinessLayer.Business;
using EntitiesLayer;
using System.Data;
using System.Reflection;

namespace WebLayer.View
{
    public partial class Index : System.Web.UI.Page
    {
        //Back de la vista principal

        // variables globales
        #region variables globales
        public static string ID = "";
        public static string Nombre="";
        public static string Apellido = "";
        public static string Edad="";
        public static string Correo = "";
        #endregion
        // metodo para guardar registros

        protected void Insertar_Click(object sender, EventArgs e)
        {
            Mostrartabla();
        }

        // metodo para consultar a detalle los registros
        protected void Detalle_Click(object sender, EventArgs e)
        {

            Cliente_PotencialSet cliente = new Cliente_PotencialSet();
            if (AId.Text != "")
            {

                Aid2.Text = ID;
                Anombre2.Text = Nombre;
                Aapellido2.Text = Apellido;
                Aedad2.Text = Edad;
                Acorreo2.Text = Correo;
                Mostrartabla();
            }
            else
            {
                string mensaje = "´Debe saber la id para consultarlo ";
                ClientScript.RegisterStartupScript(this.GetType(), "myalert", "alert('" + mensaje + "');", true);

            }

        }

        // metodo para eliminar registros de forma logica
        protected void Eliminar_Click(object sender, EventArgs e)
        {

            AId.Text = String.Empty;
            ANombre.Text = String.Empty;
            AApellido.Text = String.Empty;
            AEmail.Text = String.Empty;
            AEdad.Text = String.Empty;
            Grid.DataSource = null;
            Grid.DataBind();
        }

        // metodo para consultar varios registros
        protected void Buscar_Click(object sender, EventArgs e)
        {

            Cliente_PotencialSet cliente = new Cliente_PotencialSet();
            List<Cliente_PotencialSet> clientes = new List<Cliente_PotencialSet>();
            if (AId.Text != "")
            {

                cliente.Id = Convert.ToInt32(AId.Text);
            }

            Sales negocio = new Sales();
            string CurrentCUltura = Page.Culture;

            if (ANombre.Text != "")
            {
                cliente.Nombre = ANombre.Text;
            }

            if (AApellido.Text != "")
            {
                cliente.Apellido = AApellido.Text;
            }
            if (AEmail.Text != "")
            {
                cliente.Correo = AEmail.Text;
            }
            if (AEdad.Text != "")
            {
                cliente.Edad = AEdad.Text;
            }



            cliente.Cultura = Culture;
            cliente.Fecha_Creacion = DateTime.Now;
            clientes = negocio.GetClientes(cliente, cliente.Id);
            if(clientes.Count>0)
            {
                for(int i=0; i<clientes.Count;i++)
                {
                    cliente.Id = Convert.ToInt32(clientes[i].Id);
                    ID = cliente.Id.ToString();
                    Nombre = clientes[i].Nombre;
                    Apellido = clientes[i].Apellido;
                    Edad = clientes[i].Edad;
                    Correo = clientes[i].Correo;
                    Session["Correo"] = "2";

                }
            }
            if(clientes.Count>0)
            {
                DataTable dt=  CreateDataTable(clientes);
                Grid.DataSource = dt;
                Grid.DataBind();
            }
            else
            {
                string mensaje = "Este usuario ha sido desactivado ";
                ClientScript.RegisterStartupScript(this.GetType(), "myalert", "alert('" + mensaje + "');", true);
                Grid.DataSource = null;
                Grid.DataBind();
            }
        }

        // metodo utilitario para convertir un ienumerable a datatable
        public static DataTable CreateDataTable<T>(IEnumerable<T> list)
        {
            Type type = typeof(T);
            var properties = type.GetProperties();

            DataTable dataTable = new DataTable();
            foreach (PropertyInfo info in properties)
            {
                dataTable.Columns.Add(new DataColumn(info.Name, Nullable.GetUnderlyingType(info.PropertyType) ?? info.PropertyType));
            }

            foreach (T entity in list)
            {
                object[] values = new object[properties.Length];
                for (int i = 0; i < properties.Length; i++)
                {
                    values[i] = properties[i].GetValue(entity);
                }

                dataTable.Rows.Add(values);
            }

            return dataTable;
        }

        protected void Guardar_Click(object sender, EventArgs e)
        {
            Cliente_PotencialSet cliente = new Cliente_PotencialSet();

            cliente.Nombre = Anombre2.Text;
            cliente.Apellido = Aapellido2.Text;
            cliente.Correo = Acorreo2.Text;
            cliente.Edad = Aedad2.Text;
            cliente.Cultura = Culture;
            cliente.Estado = true;
            cliente.Fecha_Creacion = DateTime.Now;
            Sales negocio = new Sales();
            if (Aid2.Text == "")
            {
                string resultado = negocio.Save(cliente, State.Added);
                string mensaje = "Se ha creado con exito el cliente " + resultado;
                ClientScript.RegisterStartupScript(this.GetType(), "myalert", "alert('" + mensaje + "');", true);
            }
            else
            {
                cliente.Id = Convert.ToInt32(Aid2.Text);
                string resultado2 = negocio.Save(cliente, State.Modified);
                string mensaje = "Se ha actualizado con exito el cliente " + resultado2;
                ClientScript.RegisterStartupScript(this.GetType(), "myalert", "alert('" + mensaje + "');", true);
            }

            Aid2.Text = String.Empty;
            Anombre2.Text = String.Empty;
            Aapellido2.Text = String.Empty;
            Acorreo2.Text = String.Empty;
            Aedad2.Text = String.Empty;
            Ocultatabla();
        }
        //metodo para salir a la pantalla principal
        protected void Salir_Click(object sender, EventArgs e)
        {
            Aid2.Text = String.Empty;
            Anombre2.Text = String.Empty;
            Aapellido2.Text = String.Empty;
            Acorreo2.Text = String.Empty;
            Aedad2.Text = String.Empty;
            Ocultatabla();
        }
        
        //metodo para el borrado logico
        protected void Borrar_Click(object sender, EventArgs e)
        {
            Cliente_PotencialSet cliente = new Cliente_PotencialSet();
            cliente.Id = Convert.ToInt32(AId.Text);
            cliente.Nombre = Anombre2.Text;
            cliente.Apellido = Aapellido2.Text;
            cliente.Correo = Acorreo2.Text;
            cliente.Edad = Aedad2.Text;
            cliente.Cultura = Culture;
            cliente.Fecha_Creacion = DateTime.Now;
            cliente.Estado = false;
            Sales negocio = new Sales();
            string resultado = negocio.Save(cliente, State.Modified);
            string mensaje = "Se ha borrado con exito el cliente " + resultado;
            ClientScript.RegisterStartupScript(this.GetType(), "myalert", "alert('" + mensaje + "');", true);

            Aid2.Text = String.Empty;
            Anombre2.Text = String.Empty;
            Aapellido2.Text = String.Empty;
            Acorreo2.Text = String.Empty;
            Aedad2.Text = String.Empty;
            Ocultatabla();
        }

        //metodo para ocultar la otra tabla
        public void Ocultatabla()
        {
            Table1.Visible = false;
            Table2.Visible = false;
        }

        //metodo para mostrar la otra tabla
        public void Mostrartabla()
        {
            Table1.Visible = true;
            Table2.Visible = true;
        }
    }






    


}