﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using EntitiesLayer;
using DataLayer;
using Utility.Utility;


namespace BusinessLayer.Business
{
    public class Sales
    {
     //capa de negocios

        //metodo para linkear la vista al metodo getcliente para obtener un cliente
        public Cliente_PotencialSet GetCliente(string cultureName, Guid clienteID)
        {
            try
            {
                return DataLayer.Models.Repositorio_DAL.DAClientePotencial.GetCliente(cultureName, clienteID);

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //metodo para linkear la vista al metodo getclientes para obtener muchos clientes
        public List<Cliente_PotencialSet> GetClientes(Cliente_PotencialSet cliente, int clienteID)
        {
            try
            {
                return DataLayer.Models.Repositorio_DAL.DAClientePotencial.GetClientes(cliente, clienteID);

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //metodo para linkear la vista al metodo save que guarda,modifica y elimina
        public string Save(Cliente_PotencialSet cliente, State state)
        {
            try
            {
               return DataLayer.Models.Repositorio_DAL.DAClientePotencial.SaveClient(cliente, state);
                
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}