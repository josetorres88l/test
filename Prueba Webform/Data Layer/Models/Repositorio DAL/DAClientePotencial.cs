﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DataLayer.Models;
using EntitiesLayer;
using Utility.Utility;
using System.Data.Entity.Core.Objects;

namespace DataLayer.Models.Repositorio_DAL
{
    public class DAClientePotencial
    {
        //Clase de datos
        //Metodo para guardar, editar y realizar borrado logico
        public static string SaveClient(Cliente_PotencialSet cliente, State state)
        {
            try
            {
                int result = 0;
                using (var context = new CompanyEntities())
                {
                    switch (state)
                    {
                        case State.Added:

                            context.Entry(cliente).State = System.Data.Entity.EntityState.Added;
                            result = context.SaveChanges();
                            break;
                        case State.Modified:
                            context.Entry(cliente).State = System.Data.Entity.EntityState.Modified;
                            result= context.SaveChanges(); 
                            break;
                        case State.Deleted:
                            context.Entry(cliente).State = System.Data.Entity.EntityState.Modified;//borrado logico
                            result = context.SaveChanges();
                            break;
                    }
                }
                if (result > 0)
                    return cliente.Id.ToString();
                else
                    return "";
            }
            catch (System.Data.Entity.Validation.DbEntityValidationException validationException)
            {
                throw new InvalidOperationException(Excepciones.GetValidationErrors(validationException), validationException.GetBaseException());
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //Metodo para obtener un cliente
        public static Cliente_PotencialSet GetCliente(string cultureName, Guid clienteID)
        {
            try
            {
                using (var context = new  CompanyEntities())
                {
                    return context.Cliente_PotencialSet
                        .Where(c => c.Cultura.Equals(cultureName))
                        .Where(c => c.Id.Equals(clienteID))
                        .Single();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //Metodo para obtener todos los clientes que cumplan los filtros

        public static List<Cliente_PotencialSet> GetClientes(Cliente_PotencialSet cliente, int clienteID)
        {
            try
            {
                using (var context = new CompanyEntities())
                {
                    List<Cliente_PotencialSet> a2 = new List<Cliente_PotencialSet>();
                    string condicion = "";
                    

                           if (cliente.Nombre != "")
                           {
                            condicion = " Nombre = " + "'" + cliente.Nombre + "'";
                           }
                           if (cliente.Apellido != "")
                           {
                            condicion = " Apellido = " + "'" + cliente.Apellido + "'";
                           }
                           if (cliente.Edad != "")
                           {

                            condicion = " Edad = " + "'" + cliente.Edad + "'";
                           }
                           if (cliente.Correo != "")
                           {

                            condicion = " Correo = " + "'" + cliente.Correo + "'";
                           }
                           if (cliente.Id.ToString() != "")
                           {
                            condicion = " Id = " + "'"+cliente.Id+ "'" ;
                           }
                    




                    ObjectResult<Cliente_PotencialSet> a1 = context.spo_GetAllClient(" Cliente_PotencialSet ", condicion);
                  
                    
                    foreach (Cliente_PotencialSet result in a1.ToList())
                    {
                        a2.Add(result);
                    }

                    for(int i=0;i <a2.Count;i++)
                    {
                        if(a2[i].Estado==false)
                        {
                            a2.RemoveAt(i);
                        }
                    }
                    return a2;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
    
}