﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;

namespace Utility.Utility
{
    public static class Status
    {     
       
            public static System.Data.Entity.EntityState GetEquivalentEntityState(State state)
            {
                //Metodo para definir los tipos de estados
                switch (state)
                {
                    case State.Added:
                        return EntityState.Added;
                    case State.Modified:
                        return EntityState.Modified;
                    case State.Deleted:
                        return EntityState.Deleted;
                    default:
                        return EntityState.Unchanged;
                }
            }
        
    }

    public enum State
    {
        //Metodo para enumerar las acciones en los estados
        Unchanged, Added, Modified, Deleted
    }

}