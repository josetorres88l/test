﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Utility.Utility
{
    public class Excepciones
    {
        //metodo para definir y controlar excepciones
        public static string GetServiceDefaultCulture()
        {
     
            return System.Configuration.ConfigurationManager.AppSettings["DefaultCulture"];
        }
        public static string GetValidationErrors(System.Data.Entity.Validation.DbEntityValidationException validationException)
        {
            StringBuilder message = new StringBuilder();
            foreach (var eve in validationException.EntityValidationErrors)
            {
                StringBuilder valErrors = new StringBuilder();
                foreach (var ve in eve.ValidationErrors)
                {
                    valErrors.Append(" Property: " + ve.PropertyName + ", Error Message: " + ve.ErrorMessage);
                }
                message.Append(String.Format(Utility.UserMessages.ResourceManager.GetString("EntityValidationError", new System.Globalization.CultureInfo(Utility.Excepciones.GetServiceDefaultCulture())), eve.Entry.Entity.GetType().Name, eve.Entry.State, valErrors) + " ");
            }
            return message.ToString();
        }

      

    }
}